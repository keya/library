Meteor.subscribe("theBooks");

Accounts.ui.config({
    passwordSignupFields: "USERNAME_AND_EMAIL"
});

Template.borrowerForm.helpers({
    borrowerFormSchema: function() {
        return Schemas.borrowerSchema;
    }
});

Template.bookList.helpers({
    books: function () {
        return Books.find({});
    }
});

Template.borrowerForm.onRendered(function () {
    this.$("form").hide();
    
    //var bookID = Template.parentData(1)._id;
    //console.log("bookID: " + bookID);

    $(".borrowerForm .add").click(function () {
        var myParent = $(this).parent(), 
            borrowerForm = $(".borrowerForm form");

        $(".borrowerForm .add").show();
        borrowerForm.hide();
        $(this).hide();

        borrowerForm.removeClass("editing");
        myParent.find(borrowerForm).show().addClass("editing");
    });

    AutoForm.hooks({
        borrowerForm: {
            formToDoc: function( doc ) {
                doc.bookID = Template.parentData(1)._id;
                console.log(doc );
                return doc;
            }
        }
    });
    
});


